/**
 * 
 */
package com.example.ids.myappis.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ids.myappis.entities.Post;
import com.example.ids.myappis.repositories.PostRepository;


/**
 * @author idscomercial
 *
 */
@Service
public class PostServiceImpl implements PostService{
	
	@Autowired
	private PostRepository repository;

	@Override
	public List<Post> consultaPost() {
		return repository.findAll();
	}

	@Override
	public Post consultaPost(Long postID) {
		Optional<Post> post = repository.findById(postID);
		if(post.isPresent()) {
		return post.get();
	}
	return new Post();
	}

	@Override
	public void guardarPost(Post post) {
		repository.save(post);
		
	}

	@Override
	public void borrarPost(Long postID) {
		repository.deleteById(postID);
		
	}

	@Override
	public void actualizarPost(Post post) {
		repository.save(post);
		
	}
	

}
