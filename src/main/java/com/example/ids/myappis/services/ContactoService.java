/**
 * 
 */
package com.example.ids.myappis.services;

//import java.util.List;

import com.example.ids.myappis.entities.Contacto;

/**
 * @author idscomercial
 *
 */
public interface ContactoService {
//	List<Contacto> consultaAlumno();
	void guardaContacto(Contacto contacto);
}
