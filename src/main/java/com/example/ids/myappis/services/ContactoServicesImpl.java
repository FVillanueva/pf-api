/**
 * 
 */
package com.example.ids.myappis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.ids.myappis.entities.Contacto;
import com.example.ids.myappis.repositories.ContactoRepository;



/**
 * @author idscomercial
 *
 */
@Service
public class ContactoServicesImpl implements ContactoService {
	
	@Autowired
	private ContactoRepository repository;

	@Override
	public void guardaContacto(Contacto contacto) {
		// TODO Auto-generated method stub
		repository.save(contacto);
		
	}

}
