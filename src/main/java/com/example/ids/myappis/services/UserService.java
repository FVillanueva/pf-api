/**
 * 
 */
package com.example.ids.myappis.services;

import java.util.List;

import com.example.ids.myappis.entities.User;

/**
 * @author idscomercial
 *
 */
public interface UserService {
	List<User> consultaUser();
	User consultaUser(Long userID);
	void guardarUser(User user);
	void borrarUser(Long userID);
	void actualizarUser(User user);

}
