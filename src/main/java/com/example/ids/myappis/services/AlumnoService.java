/**
 * 
 */
package com.example.ids.myappis.services;

import java.util.List;

import com.example.ids.myappis.entities.Alumno;

/**
 * @author idscomercial
 *
 */
public interface AlumnoService {
	List<Alumno> consultaAlumno();
	Alumno consultaAlumno(Long alumnoID);
	void guardaAlumno(Alumno alumno);
	void borrarAlumno(Long alumnoID);
	void actualizarAlumno(Alumno alumno);
}
