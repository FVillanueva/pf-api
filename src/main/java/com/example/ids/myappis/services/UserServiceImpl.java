/**
 * 
 */
package com.example.ids.myappis.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ids.myappis.entities.User;
import com.example.ids.myappis.repositories.UserRepository;

/**
 * @author idscomercial
 *
 */
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository repository;

	@Override
	public List<User> consultaUser() {
		return repository.findAll();
	}

	@Override
	public User consultaUser(Long userID) {
		Optional<User> user = repository.findById(userID);
			if(user.isPresent()) {
			return user.get();
		}
		return new User();
	}
		
	

	@Override
	public void guardarUser(User user) {
		repository.save(user);
		
	}

	@Override
	public void borrarUser(Long userID) {
		repository.deleteById(userID);
		
	}

	@Override
	public void actualizarUser(User user) {
		repository.save(user);
		
	}

}
