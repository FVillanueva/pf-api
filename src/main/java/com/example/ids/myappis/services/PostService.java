/**
 * 
 */
package com.example.ids.myappis.services;

import java.util.List;

import com.example.ids.myappis.entities.Post;

/**
 * @author idscomercial
 *
 */
public interface PostService {
	List<Post> consultaPost();
	Post consultaPost(Long postID);
	void guardarPost(Post post);
	void borrarPost(Long postID);
	void actualizarPost(Post post);
}
