/**
 * 
 */
package com.example.ids.myappis.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ids.myappis.entities.Alumno;
import com.example.ids.myappis.repositories.AlumnoRepository;

/**
 * @author idscomercial
 *
 */
@Service
public class AlumnoServicesImpl	implements AlumnoService {
	
	@Autowired
	private AlumnoRepository repository;

	@Override
	public List<Alumno> consultaAlumno() {
		return repository.findAll();
	}

	@Override
	public Alumno consultaAlumno(Long alumnoID) {
		Optional<Alumno> alumno = repository.findById(alumnoID);
		if(alumno.isPresent()) {
			return alumno.get();
		}
		return new Alumno();
	}

	@Override
	public void guardaAlumno(Alumno alumno) {
		repository.save(alumno);
		
	}

	@Override
	public void borrarAlumno(Long alumnoID) {
		repository.deleteById(alumnoID);
		
	}

	@Override
	public void actualizarAlumno(Alumno alumno) {
		repository.save(alumno);
		
	}
}
