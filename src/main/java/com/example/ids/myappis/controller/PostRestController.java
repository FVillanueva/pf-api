/**
 * 
 */
package com.example.ids.myappis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.ids.myappis.entities.Post;
import com.example.ids.myappis.services.PostService;



/**
 * @author idscomercial
 *
 */
@RestController
@CrossOrigin
public class PostRestController {
	
	@Autowired
	private PostService service;
	
	@GetMapping("api/post")
	public List<Post> consultaPost(){
		List<Post> post = service.consultaPost();
		System.out.println("Lista de Post" + post);
		return post;
	}
	
	@GetMapping("/api/post/{postId}")
	public Post consultaPost(@PathVariable(name="postId") Long postID) {
		Post post = service.consultaPost(postID);
		System.out.println("Post encontrado: " + post);
		return post;
	}
	
	
	@PostMapping("/api/post")
	public void guardarPost(@RequestBody Post post) {
		service.guardarPost(post);
		System.out.println("Post guardado");
	}
	
	@DeleteMapping("/api/post/{postId}")
	public void borrarPost(@PathVariable(name="postId") Long postID) {
		service.borrarPost(postID);
		System.out.println("Post borrado: " + postID);
	}
	
	@PutMapping("/api/post/{postId}")
	public void actualizarPost(@RequestBody Post post, 
			@PathVariable(name="postId") Long postId) {
		service.guardarPost(post);
		System.out.println("Post Actualizado");
	}

}
