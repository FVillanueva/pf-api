/**
 * 
 */
package com.example.ids.myappis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.ids.myappis.entities.Contacto;
import com.example.ids.myappis.services.ContactoService;

/**
 * @author idscomercial
 *
 */
@RestController
@CrossOrigin
public class ContactoRestController {
	
	@Autowired
	private ContactoService service;
	
	@PostMapping("/api/contacto")
	public void guardaContacto(@RequestBody Contacto contacto) {
		service.guardaContacto(contacto);
		System.out.println("Contacto guardado");
	}
}
