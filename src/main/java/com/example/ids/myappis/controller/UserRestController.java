/**
 * 
 */
package com.example.ids.myappis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.example.ids.myappis.entities.User;
import com.example.ids.myappis.services.UserService;

/**
 * @author idscomercial
 *
 */
@RestController
@CrossOrigin
public class UserRestController {
	
	@Autowired
	private UserService service;
	
	@GetMapping("api/user")
	public List<User> consultaUser(){
		List<User> user = service.consultaUser();
		System.out.println("Lista de alumnos" + user);
		return user;
	}
	
	@GetMapping("/api/user/{userId}")
	public User consultaUser(@PathVariable(name="userId") Long userID) {
		User user = service.consultaUser(userID);
		System.out.println("Alumno encontrado: " + user);
		return user;
	}
	
	
	@PostMapping("/api/user")
	public void guardarUser(@RequestBody User user) {
		service.guardarUser(user);
		System.out.println("User guardado");
	}
	
	@DeleteMapping("/api/user/{userId}")
	public void borrarUser(@PathVariable(name="userId") Long userID) {
		service.borrarUser(userID);
		System.out.println("User borrado: " + userID);
	}
	
	@PutMapping("/api/user/{userId}")
	public void actualizarUser(@RequestBody User user, 
			@PathVariable(name="userId") Long userId) {
		service.guardarUser(user);
		System.out.println("User Actualizado");
	}
}
