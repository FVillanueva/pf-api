/**
 * 
 */
package com.example.ids.myappis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ids.myappis.entities.Contacto;

/**
 * @author idscomercial
 *
 */
@Repository
public interface ContactoRepository extends JpaRepository<Contacto, Long>{

}
